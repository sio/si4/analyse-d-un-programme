% Analyse d'un programme exécutable
%
% Auteur : Grégory DAVID
%%

\documentclass[12pt,a4paper,oneside,titlepage,final]{article}

\usepackage{style/layout}
\newcommand{\PROGRAMDIR}{data}
\usepackage[hide=true,dir=\PROGRAMDIR]{bashful}
\lstdefinestyle{bashfulScript}{basicstyle=\scriptsize\ttfamily,emptylines=1}
\lstdefinestyle{bashfulStdout}{basicstyle=\scriptsize\ttfamily,emptylines=1}
\lstset{basicstyle={\scriptsize\ttfamily},emptylines=1}

\newcommand{\MONTITRE}{Analyse d'un programme exécutable}
\newcommand{\MONSOUSTITRE}{comme un chirurgien}
\newcommand{\DISCIPLINE}{\glsentrytext{SiQUATRE} -- \glsentrydesc{SiQUATRE}}

\usepackage[%
	pdftex,%
	pdfpagelabels=true,%
	pdftitle={\MONTITRE},%
	pdfauthor={Grégory DAVID},%
	pdfsubject={\MONSOUSTITRE},%
        colorlinks,%
]{hyperref}
\usepackage{style/glossaire}
\addto\captionsfrench{%
  \renewcommand*{\glossaryname}{Lexique}%
  \renewcommand*{\acronymname}{Liste des acronymes}%
}
\usepackage{style/commands}

\longnewglossaryentry{en tete}{name=en-tête, sort={en-tete}}{L'en-tête
  d'un module \texttt{C++} permet de définir les bibliothèques
  externes à utiliser. Voir \lstlistingname{} \vref{lst:header}.

  \lstinputlisting[language={C++},%
  caption={Exemple d'en-tête},%
  label={lst:header},%
  linerange=En\-tête-Fin]{\PROGRAMDIR/src/structure_programme.cpp} }

\longnewglossaryentry{namespace}{name=espace de nom, plural={espaces
    de nom}}{Les espaces de nom du code source \texttt{C++} permettent
  d'accéder de manière moins verbeuse aux fonctions des bibliothèques
  externes incluses elles-mêmes dans des espaces de nom. Voir
  \lstlistingname{} \vref{lst:namespace}.

  \lstinputlisting[language={C++},%
  caption={Exemple d'espace de nom},%
  label={lst:namespace},%
  linerange=Espaces\ de\ nom-Fin%
  ]{\PROGRAMDIR/src/structure_programme.cpp} }

\longnewglossaryentry{main}{name=programme principal,
  plural={programmes principaux}}{Le programme principal du code
  source \texttt{C++} est la fonction d'entrée dans le programme. Elle
  doit toujours s'appeler \texttt{main} et doit retourner un nombre
  entier \texttt{int} qui représente son code de sortie (\texttt{0}
  étant par convention la valeur de la réussite, et toute autre valeur
  symbolise l'échec dont la valeur permet de distinguer l'erreur).
  Voir \lstlistingname{} \vref{lst:main}.

  \lstinputlisting[language={C++},%
  caption={Exemple de programme principal},%
  label={lst:main},%
  linerange=Programme\ principal-Fin%
  ]{\PROGRAMDIR/src/structure_programme.cpp} }


\longnewglossaryentry{structure generale}{name=structure g{é}n{é}rale
  d'un module, plural={structures générales des modules}, see={en
    tete,namespace,main}}{La structure générale d'un module
  \texttt{C++} reprend les éléments décrits pièce par pièce.

  \lstinputlisting[language={C++},%
  caption={Un programme écrit en \texttt{C++} se structure de la façon
    suivante},%
  label={lst:structure.generale}]{\PROGRAMDIR/src/structure_programme.cpp}}


\newacronym{gdb}{GDB}{The \gls{GNU} {D}e{B}ugger}
\newacronym[sort=objdump]{objdump}{\texttt{objdump}}{{O}bject {D}umper}

\title{%
  \begin{flushright}
      \noindent{\Huge {\bf \MONTITRE}} \\
      \noindent{\huge \MONSOUSTITRE} \\
      \noindent{\large \DISCIPLINE} \\
  \end{flushright}
}

\newcommand{\SOURCEPATH}{/home/p.nom/si4/analyse-programme}

\lstdefinelanguage{gdb}
{
  morecomment=[f]{$},%$
  morecomment=[f]{0x},
  keywords={%
    backtrace,
    break,
    clear,
    condition,
    continue,
    delete,
    directory,
    disable,
    disassemble,
    display,
    enable,
    finish,
    frame,
    handle,
    info,
    kill,
    list,
    next,
    print,
    quit,
    return,
    run,
    set,
    step,
    thread,
    watch,
    whatis,
    where,
  },
  moredelim=[s][\color{DarkRed}]{x/}{\ },
  sensitive=true,
}

\begin{document}

\lstset{%
  rangeprefix=/*\ ,%
  rangesuffix=\ */,%
  includerangemarker=false%
}%

% Page de titre
\maketitle

% Copyright
\include{copyright}

\newpage
\vspace*{\fill}
\tableofcontents{}
\vspace*{\fill}

% Contenu
\HEADER{\MONTITRE}{\DISCIPLINE}

\clearpage %
\glsadd{en tete}
\glsadd{namespace}
\glsadd{main}
\glsadd{structure generale}
\label{sec:glossaire}
\printglossary[toctitle={\glossaryname}]

\label{sec:acronymes}
\printacronyms[toctitle={\acronymname}]

\clearpage %
\section{Contexte}\label{sec:contexte}
Dans l'ensemble des sections suivantes, nous baserons nos analyses sur
le code source simple suivant
(\lstlistingname{}~\vref{lst:calculsimple.source}), et le résultat de
sa construction.

\lstinputlisting[language={C++},label={lst:calculsimple.source},
caption={Code source du programme sauvegardé dans
  \texttt{\SOURCEPATH/\lstname}}]{\PROGRAMDIR/src/calcul_simple.cpp}

\section{Analyse statique du code machine
  généré}\label{sec:analyse.statique}

\subsection{Construction jusqu'à
  l'assemblage}\label{sec:analyse.statique.construction}

\paragraph{Structure du projet} ~ \lstset{caption={Structure initiale
    du projet de développement}, label={lst:structure.data}}%

\bash[stdout,ignoreExitCode=true,ignoreStderr=true,prefix={}]
rm bin/*
rm obj/*.o
rm src/*.{ii,s}
tree -i -n -f -l -F -I "*\~|make*|*.gin|analyse*|struct*|trouv*|*.gdb"
\END

\lstset{language={sh}, caption={Invocation de la construction et arrêt
    à l'étape d'assemblage}, label={lst:assemblage}}%

\bash[script,ignoreExitCode=true,ignoreStderr=true,prefix={}]
# l'option -ggdb permet d'integrer des informations
# de debugging dans le fichier .o
# l'option -O0 permet d'interdire les optimisations
# nuisibles au debuggage
g++ -ggdb -O0 -c src/calcul_simple.cpp -o obj/calcul_simple.o
\END

\subsection{Analyse statique avec
  \gls{objdump}}\label{sec:analyse.statique.objdump}

L'utilitaire \gls{objdump} permet d'obtenir un ensemble d'informations
substantielles à propos du fichier objet résultant de l'étape
d'assemblage.

Nous pouvons l'utiliser pour afficher le code machine et les
instructions assembleur d'un programme ayant été assemblé. Une
construction avec l'option \texttt{-g} de \gls{gdb} permet en plus
d'entrelacer le code source écrit en \texttt{C++} avec le code
machine, tout comme montré dans le \lstlistingname~\vref{lst:objdump}.

\begin{lstlisting}[language={sh}, caption={Invocation de l'analyseur
statique de fichier objet}, label={lst:assemblage}]
objdump --disassemble --source obj/calcul_simple.o
\end{lstlisting}

\bash[ignoreExitCode=true,ignoreStderr=true,prefix={}]
LANG='' objdump --disassemble --source obj/calcul_simple.o | sed -e '/^$/d'
\END %$


\lstinputlisting[language={}, label={lst:objdump}, caption={Affichage
  avec \gls{objdump} du programme assemblé dans
  \texttt{obj/calcul\_simple.o}, du code source en \texttt{C++} et des
  instructions assembleur}]{\PROGRAMDIR/\jobname.stdout}

Nous pouvons, dans le contexte du \lstlistingname~\vref{lst:objdump},
en déduire par exemple que l'instruction assembleur \texttt{movb} sert
à effectuer une affectation d'une valeur à une variable. Nous pouvons
aussi identifier que le code machine de cette instruction est
\texttt{c645}, car ce code machine est visible à chaque fois que
l'instruction assembleur \texttt{movb} est utilisée (Voir les lignes
8, 10, 12 et 14 notamment).

\clearpage %
\section{Analyse dynamique de l'exécution avec
  \gls{gdb}}\label{sec:analyse.dynamique}

\subsection{Définition}\label{sec:debugger.definition}
\gls{gdb} est un \emph{debugger} (ou débugueur, ou débogueur). Ce qui
fait de lui un logiciel qui aide un développeur à analyser un
programme ainsi que ses \emph{bugs}. Pour cela, il permet d'exécuter
le programme pas-à-pas, d'afficher la valeur des variables à tout
moment, de mettre en place des points d'arrêt sur des conditions ou
sur des lignes du programme, et d'altérer le programme au cours de son
exécution.

Il s'agit de l'application à la programmation informatique du
processus d'aide au dépannage (en anglais \emph{troubleshooting}) qui
désigne un processus de recherche logique et systématique de
résolution de problèmes. Ce processus passe par la recherche de la
cause d'un problème jusqu'à sa résolution et la remise en marche du
procédé ou du produit.

Ce type de processus est nécessairement mis en place dans le
développement et la maintenance de systèmes complexes, dans lesquels
un même problème peut avoir des causes multiples ou
différentes. L'aide au dépannage est utilisée par exemple en
ingénierie, en électronique, dans le développement de programmes
informatiques (où il prend le nom didactique de débogueur), dans la
gestion de réseaux informatiques et en diagnostic médical.

L'aide au dépannage consiste initialement en l'identification des
dysfonctionnements par élimination progressive des causes connues.

Le programme à débuguer est alors exécuté à travers le débogueur et
s'exécute normalement. Le débogueur offre alors au programmeur la
possibilité d'\textbf{observer} et de \textbf{contrôler} l'exécution
du programme, en lui permettant par divers moyens de l'observer, de la
stopper (mettre en pause l'exécution du programme) et de la
changer. Par exemple, la pile d’exécution et le contenu des variables
en mémoire peuvent être observés, et la valeur des variables peut être
changée pour altérer le flot de contrôle du programme afin, par
exemple, de \textbf{déterminer la cause d'une défaillance}.

Quand l'exécution d'un programme est stoppée, le débogueur affiche la
position courante d'exécution dans le code source original pour le
débogage formel et le débogage au niveau source. Le débogueur de bas
niveau ou de niveau machine montre la ligne désassemblée.

De nombreux débogueurs permettent, en plus de l'observation de l'état
des registres processeurs et de la mémoire, de les modifier avant de
rendre la main au programme débogué. Ils peuvent alors être utilisés
pour localiser certaines protections logicielles et les désactiver,
amenant à la conception d'un \emph{crack}. Ainsi, certains logiciels,
connaissant le fonctionnement des débogueurs et voulant empêcher de
telles modifications, mettent en place des techniques antidébogage
(\emph{anti debugging tricks}).

\subsection[Les commandes de \gls{gdb}]{Les commandes à utiliser dans
  \gls{gdb}, et à retenir}\label{sec:debugger.commandes}
\begin{description}
  \item[\texttt{help}] permet d'obtenir de l'aide à propos d'une
  instruction, par exemple : \texttt{help x} ou bien \texttt{help
    break}

  \item[\texttt{break}] permet de positionner un point d'arrêt
  \emph{breakpoint} sur une ligne ou une fonction du programme, par
  exemple : \texttt{break main}

  \item[\texttt{list}] permet d'afficher le code source associé au
  programme (si toutefois le programme a été compilé avec l'option
  \texttt{-g})

  \item[\texttt{run}] permet de lancer l'exécution par \gls{gdb} du
  programme

  \item[\texttt{disassemble}] permet d'afficher les instructions
  d'assemblage et le code machine du programme

  \item[\texttt{step}] permet de réaliser la ligne d'instruction
  \texttt{C++} précédemment affichée, et de positionner le curseur
  d'exécution sur la prochaine ligne d'instruction \texttt{C++} et
  d'attendre

  \item[\texttt{whatis}] permet d'obtenir le type d'une variable
  fournie en paramètre : \texttt{whatis my\_variable}

  \item[\texttt{print}] permet d'afficher ponctuellement le contenu
  d'une variable, par exemple : \texttt{print ma\_variable}

  \item[\texttt{display}] permet de définir l'affichage d'une variable
  à chaque pas dans le déroulé du programme : \texttt{display
    ma\_variable}

  \item[\texttt{x/nuf}] permet de réaliser un examen approfondi d'une
  adresse mémoire en spécifiant pour \texttt{n} le nombre d'éléments à
  considérer, \texttt{u} la taille de l'élément à examiner et
  \texttt{f} le façon d'assurer le décodage de l'élément, des
  paramètres de décodage de l'information (voir la \emph{cheat sheet}
  sur \gls{gdb}), par exemple : \texttt{x/4bc 0x7ffffffe4e8} permet de
  décoder en tant que caractère (\texttt{f=c}) les 4 (\texttt{n=4})
  octets (\texttt{u=b}) depuis le bloc mémoire d'adresse
  \texttt{0x7ffffffe4e8}

  \item[\texttt{quit}] permet de quitter la session \gls{gdb}
\end{description}

\subsection{Session de \emph{debugging}}\label{sec:debugger.debugging}
\lstset{caption={Contenu et état du répertoire de travail
    \texttt{\SOURCEPATH/\PROGRAMDIR}}, label={lst:debugging.data}}%

\bash[stdout,ignoreExitCode=true,ignoreStderr=true,prefix={}]
rm bin/*
tree -i -n -f -l -F -P "calcul_simple.cpp|calcul_simple.o"
\END

\lstset{language={sh}, caption={Invocation de l'édition des liens pour
    terminer la construction de l'exécutable},
  label={lst:edition.des.liens}}%

\bash[script,ignoreExitCode=true,ignoreStderr=true,prefix={}]
g++ -ggdb -O0 obj/calcul_simple.o -o bin/calcul_simple
\END

\lstset{caption={Contenu et état du répertoire de travail
    \texttt{\SOURCEPATH/\PROGRAMDIR} après
    \lstlistingname~\ref{lst:edition.des.liens}},
  label={lst:debugging.execution.data}}%

\bash[stdout,ignoreExitCode=true,ignoreStderr=true,prefix={}]
tree -i -n -f -l -F -P "calcul_simple.cpp|calcul_simple.o|calcul_simple"
\END

À partir du code source disponible dans le
\lstlistingname~\vref{lst:calculsimple.source} et du résultat de sa
construction en un exécutable nommé \texttt{calcul\_simple}, nous
invoquons le \emph{debugging} du programme tel que décrit dans le
\lstlistingname~\vref{lst:calculsimple.gdb.launch}.

\begin{lstlisting}[%
caption={Invocation du \emph{debugging} sur le fichier exécutable},
label={lst:calculsimple.gdb.launch}]
gdb bin/calcul_simple
\end{lstlisting}

Les sessions de \emph{debugging} présentées dans les
\lstlistingname~\vrefrange{lst:debugger.debugging.session.simple}{lst:debugger.debugging.session.alterations}
donne la direction d'usage de \gls{gdb} pour analyser dynamiquement un
programme.

\subsubsection{Déroulé simple}\label{sec:deroule.simple}
Le \lstlistingname~\vref{lst:debugger.debugging.session.simple}
présente l'usage des commandes \gls{gdb} : \texttt{break},
\texttt{run}, \texttt{list}, \texttt{step}, \texttt{continue} et
\texttt{quit}, dans le cadre d'un déroulé simple (pas à pas) de
l'analyse dynamique d'un programme.

\lstset{language={gdb}, basicstyle={\ttfamily\scriptsize},
  label={lst:debugger.debugging.session.simple}, caption={Exemple de
    session \gls{gdb}, sur l'exécutable issu de la construction de
    \texttt{\SOURCEPATH/data/src/calcul\_simple.cpp}, réalisant un
    déroulé simple du programme}}

\bash[stdout,ignoreExitCode=true,ignoreStderr=true,prefix={}]
cat > bin/calcul_simple.gdb << EOF
set trace-commands on
break main
run
list
step
step
step
step
step
step
continue
quit
EOF
gdb -batch -x bin/calcul_simple.gdb bin/calcul_simple | sed -e 's/^+/(gdb) /g'
\END

\clearpage %
\subsubsection{Examen des variables}\label{sec:examination.variables}

\paragraph{Affichage de l'interprétation de la valeur d'une variable} ~
Toute variable peut être affichée selon l'interprétation de son
type. Si la variable est de type \texttt{long long int}, alors
\gls{gdb} réalisera l'interprétation de la valeur numérique contenu à
l'adresse mémoire de la variable comme un entier relatif codé sur
\texttt{64 bits}.

Le \lstlistingname~\vref{lst:debugger.debugging.session.print}
présente l'usage le plus simple de la commande \texttt{print} qui
permet d'afficher une interprétation de la valeur d'une variable.

\lstset{language={gdb}, basicstyle={\ttfamily\scriptsize},
  label={lst:debugger.debugging.session.print}, caption={Session
    \gls{gdb} montrant l'usage de la commande \texttt{print}}}

\bash[stdout,ignoreExitCode=true,ignoreStderr=true,prefix={}]
cat > bin/calcul_simple.gdb << EOF
set trace-commands on
break main
run
list
print a
print b
print c
print d
step
print a
step
print b
step
print c
step
print d
print resultat
step
print resultat
continue
quit
EOF
gdb -batch -x bin/calcul_simple.gdb bin/calcul_simple | sed -e 's/^+/(gdb) /g'
\END

\clearpage %
\paragraph{Affichage de l'adresse d'une variable} ~ %
Le \lstlistingname~\vref{lst:debugger.debugging.session.print.adresse}
présente le moyen d'obtenir l'adresse d'une variable (exactement comme
dans le langage \texttt{C++}).

\lstset{language={gdb}, basicstyle={\ttfamily\scriptsize},
  label={lst:debugger.debugging.session.print.adresse},
  caption={Session \gls{gdb} montrant l'usage de la commande
    \texttt{print} pour afficher l'adresse des variables}}

\bash[stdout,ignoreExitCode=true,ignoreStderr=true,prefix={}]
cat > bin/calcul_simple.gdb << EOF
set trace-commands on
break main
run
list
print &a
step
print &a
step
step
step
print &resultat
continue
quit
EOF
gdb -batch -x bin/calcul_simple.gdb bin/calcul_simple | sed -e 's/^+/(gdb) /g'
\END

\clearpage %
\paragraph{Examen de la mémoire à l'adresse d'une variable} ~ %
Le \lstlistingname~\vref{lst:debugger.debugging.session.print.memoire}
présente des moyens d'examiner la mémoire d'une variable ou depuis
l'adresse d'une variable.

\lstset{language={gdb}, basicstyle={\ttfamily\scriptsize},
  label={lst:debugger.debugging.session.print.memoire},
  caption={Session \gls{gdb} montrant l'usage de la commande
    \texttt{print} pour afficher l'adresse des variables}}

\bash[stdout,ignoreExitCode=true,ignoreStderr=true,prefix={}]
cat > bin/calcul_simple.gdb << EOF
set trace-commands on
break main
run
list
step
x/1cb &a
x/1db &a
x/1tb &a
step
step
step
x/1cb &d
x/4db &d
x/4cb &d
x/4tb &d
x/1dh &d
x/1dw &d
step
print resultat
x/1dw &resultat
x/1tw &resultat
x/2dh &resultat
x/2th &resultat
x/4db &resultat
x/4tb &resultat
continue
quit
EOF
gdb -batch -x bin/calcul_simple.gdb bin/calcul_simple | sed -e 's/^+/(gdb) /g'
\END

\clearpage %
\paragraph{Affichage continu de l'interprétation des valeurs des
  variables}

Il peut être intéressant de suivre l'état de certaines variables au
fur et à mesure du déroulé du programme. Dans ce cas, nous pourrions
envisager d'utiliser la commande \texttt{print} à chaque pas
(\texttt{step}).

Autrement, nous pouvons avantageusement exploiter la commande
\texttt{display}, tel que présenté dans le
\lstlistingname~\vref{lst:debugger.debugging.session.display}.

\lstset{language={gdb}, basicstyle={\ttfamily\scriptsize},
  label={lst:debugger.debugging.session.display}, caption={Session
    \gls{gdb} montrant l'usage de la commande \texttt{display}}}

\bash[stdout,ignoreExitCode=true,ignoreStderr=true,prefix={}]
cat > bin/calcul_simple.gdb << EOF
set trace-commands on
break main
run
display a
display b
display c
display d
display resultat
step
step
step
step
step
step
continue
quit
EOF
gdb -batch -x bin/calcul_simple.gdb bin/calcul_simple | sed -e 's/^+/(gdb) /g'
\END


\subsubsection{Altérations pendant
  l'exécution}\label{sec:alterations.pendant.lexecution}

Visualiser la mémoire (voir \vref{sec:examination.variables}) ou
dérouler l'exécution d'un programme (voir \vref{sec:deroule.simple})
est l'utilité élémentaire de \gls{gdb} -- ou de n'importe quel
débogueur.

Par ailleurs, être en mesure d'altérer un programme au cours de son
exécution est une tâche particulièrement intéressante qu'offre
\gls{gdb}. L'exemple présenté dans le
\lstlistingname~\vref{lst:debugger.debugging.session.alterations}
montre comment réaliser ceci, en modifiant la valeur d'une variable.

\lstset{language={gdb}, basicstyle={\ttfamily\scriptsize},
  label={lst:debugger.debugging.session.alterations}, caption={Session
    \gls{gdb} illustrant une altération de la variable \texttt{b} au
    cours du déroulé du programme}}

\bash[stdout,ignoreExitCode=true,ignoreStderr=true,prefix={}]
cat > bin/calcul_simple.gdb << EOF
set trace-commands on
break main
run
list
step
step
print b
set var b=0
print b
step
step
step
print resultat
info locals
continue
quit
EOF
gdb -batch -x bin/calcul_simple.gdb bin/calcul_simple | sed -e 's/^+/(gdb) /g'
\END

\clearpage %
\section{Exercice}\label{sec:exercice}
À partir de l'exécutable \texttt{bin/trouver\_password} -- résultant
de la \textbf{construction} du fichier source \texttt{C++} présenté
dans le \lstlistingname~\vref{lst:trouverpassword.source} \textbf{que
  vous avez à réaliser} -- effectuer l'analyse dynamique du programme
et en déduire ce que le programmeur a dissimulé dans la variable
\texttt{mot\_de\_passe}.

\lstinputlisting[language={C++},%
caption={\texttt{\SOURCEPATH/\lstname}},%
label={lst:trouverpassword.source},%
rangeprefix=/*\ ,rangesuffix=\ */,includerangemarker=false,%
linerange=sujet-end,firstnumber=0]{\PROGRAMDIR/src/trouver_password.cpp}

\end{document}
