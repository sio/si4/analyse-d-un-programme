/*
  Structure d'un programme en C++
*/

/* En-tête */
#include <iostream>
// référence à une bibliothèque globale connue
// généralement dans '/usr/include'

#include "../h/machin.h"
// référence à une bibliothèque relativement
// au fichier courant
/* Fin */

/* Espaces de nom */
using namespace std;
/* Fin */

/* Programme principal */
int main(){
  // Écrire ici les instructions du programme

  // Code de sortie du programme
  // La valeur 0 est la convention pour la réussite
  return 0;
}
/* Fin */
