GNU gdb (Debian 7.12-6) 7.12.0.20161007-git
Copyright (C) 2016 Free Software Foundation, Inc.
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.  Type "show copying"
and "show warranty" for details.
This GDB was configured as "x86_64-linux-gnu".
Type "show configuration" for configuration details.
For bug reporting instructions, please see:
<http://www.gnu.org/software/gdb/bugs/>.
Find the GDB manual and other documentation resources online at:
<http://www.gnu.org/software/gdb/documentation/>.
For help, type "help".
Type "apropos word" to search for commands related to "word"...
Reading symbols from bin/calcul_simple...done.
(gdb) break main
Breakpoint 1 at 0x61e: file calcul_simple.cpp, line 3.
(gdb) run
Starting program: /home/p.nom/si4/analyse-programme/data/bin/calcul_simple 
Breakpoint 1, main () at calcul_simple.cpp:3
3	  a = 83;
(gdb) print a
$1 = 0 '\000'
(gdb) next
4	  b = 84;
(gdb) print a
$2 = 83 'S'
(gdb) print &a
$3 = 0x7fffffffdfbf "S`FUUUU"
(gdb) next
5	  c = 83;
(gdb) next
6	  d = 49;
(gdb) next
8	  resultat = a * b + c * d;
(gdb) print resultat
$4 = 140737488347296
(gdb) print &resultat
$5 = (long *) 0x7fffffffdfb0
(gdb) x/1gd 0x7fffffffdfb0
0x7fffffffdfb0:	140737488347296
(gdb) next
9	  return 0;
(gdb) x/1gd 0x7fffffffdfb0
0x7fffffffdfb0:	11039
(gdb) next
10	}
(gdb) next
__libc_start_main (main=0x55555555461a <main()>, argc=1, argv=0x7fffffffe0a8, init=<optimized out>, 
    fini=<optimized out>, rtld_fini=<optimized out>, stack_end=0x7fffffffe098) at ../csu/libc-start.c:325
(gdb) quit
A debugging session is active.

	Inferior 1 [process 27458] will be killed.

Quit anyway? (y or n) y
