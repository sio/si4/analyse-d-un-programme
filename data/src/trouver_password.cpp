#include <string>
#include <iostream>
void solution(long int * mdp){
  std::string solution;
  for(std::size_t i = 0; i < 8; i++){
    solution.append(1, *((char *)mdp + i));
  }
  std::cout << solution << std::endl;
}

/* sujet */
int main(){
  long int mot_de_passe;
  mot_de_passe = 0x42a866a48666a642;
  mot_de_passe = mot_de_passe >> 1;
  return 0;
}
/* end */
